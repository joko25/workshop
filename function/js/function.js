function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

function reg_btnch(){

};

function regist(){
  var full_name = $('#reg_full_name').val();
  var username = $('#reg_username').val();
  var email = $('#reg_email').val();
  var password = $('#reg_password').val();
  var repassword = $('#reg_repassword').val();
  var agre = "";
  var result = "";
  if (validateEmail(email)) {
    //console.log('email is available');
  }else{
    //console.log('email is not available');
    $('#reg_email').focus();
    result = result+'Email is not available <br/>';
  };

  if (repassword != password) {
    result = result+'Password not same <br/>';
    $('#repassword').val('');
  };

  if ($('#reg_agre').is(':checked')) {
    //agre = "true";
    //$('#btn_register').enabled();
    //$('#btn_register').disabled = false;
  }else {
    result = result+'Please Agree the terms <br/>';
    //agre = "false";  
    //$('#btn_register').disabled = true;       
  };

  if (result !="") {
    $("#reg_resulterror").html(result);
    $("#reg_error").show();
    $("#reg_success").hide();
  }else{
    $("#reg_error").hide();
    $("#reg_success").show();

    $.post("action/save/save_regist.php",{
      full_name: full_name,
      username: username,
      email: email,
      password: password
    }).done(function(res){
      var result = eval('('+res+')');
      console.log(result);
    });
  }
  //console.log(full_name+' '+email+' '+password+' '+repassword+' '+agre);
}

 function login(){
    var username = $('#username').val();
    var password = $('#password').val();
    console.log(username+' '+password);

    $.post('check_login.php', {
      username: username,
      password: password
    }).done(function(res){
      var result = eval('('+res+')');
      if (result.errorMsg) {
        console.log('ga boleh masuk');
        $('#log_error').show();
        $('#log_resulterror').text("Username or Password is Wrong!");
      }else{
        $('#log_error').hide();
        console.log('masuk');
        location.href="index.php";
      }
    });
  }